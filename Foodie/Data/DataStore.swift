//
//  DataStore.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/7/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import Foundation
import CoreData

class DataStore {
    
    fileprivate static let seedVersionDefaultsKey = "Foodie Seed Version"
    fileprivate static let currentSeedVersion = 1
    
    public static let sharedInstance = DataStore()
    
    init() {
        seedDataStoreIfNeeded()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Foodie")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func seedDataStoreIfNeeded() {
        let defaults = UserDefaults.standard
        
        let seedVersion = defaults.seedVersion
        if seedVersion < DataStore.currentSeedVersion {
            
            let defaultCuisines = [
                "African",
                "American",
                "BBQ",
                "British",
                "Cajun",
                "Caribbean",
                "Chinese",
                "French",
                "German",
                "Greek",
                "Indian cuisine",
                "Indonesian",
                "Italian",
                "Jamaican",
                "Japanese",
                "Lebanese",
                "Mediterranean",
                "Mexican",
                "Middle Eastern",
                "Moroccan",
                "Peruvian",
                "Puerto Rican",
                "Russian",
                "Seafood",
                "Soul food",
                "Taiwanese",
                "Tapas",
                "Thai",
                "Turkish",
                "Vietnamese"
            ]
            
            let context = persistentContainer.newBackgroundContext()
            
            for name in defaultCuisines {
                let cuisine = Cuisine(entity: Cuisine.entity(), insertInto: context)
                cuisine.name = name
            }
            
            do {
                try context.save()
                defaults.seedVersion = DataStore.currentSeedVersion
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

public extension UserDefaults {
    var seedVersion: Int {
        get {
            return self.integer(forKey: DataStore.seedVersionDefaultsKey)
        }
        set {
            self.set(newValue, forKey: DataStore.seedVersionDefaultsKey)
        }
    }
}
