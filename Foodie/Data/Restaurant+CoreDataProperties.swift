//
//  Restaurant+CoreDataProperties.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/9/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//
//

import Foundation
import CoreData

extension Restaurant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Restaurant> {
        return NSFetchRequest<Restaurant>(entityName: "Restaurant")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var averageRating: Double
    @NSManaged public var cuisine: Cuisine?
    @NSManaged public var reviews: NSSet?
    @NSManaged public var mostRecentReview: Date?
    
    func addReview(_ review: Review) {
        addToReviews(review)
        
        // Update Average Rating
        let average = reviews?.value(forKeyPath: "@avg.rating.floatValue") as? Double ?? 0.0
        averageRating = average
        
        let maxDate = reviews?.value(forKeyPath: "@max.date") as? Date
        mostRecentReview = maxDate
    }
}

// MARK: Generated accessors for reviews
extension Restaurant {

    @objc(addReviewsObject:)
    @NSManaged public func addToReviews(_ value: Review)

    @objc(removeReviewsObject:)
    @NSManaged public func removeFromReviews(_ value: Review)

    @objc(addReviews:)
    @NSManaged public func addToReviews(_ values: NSSet)

    @objc(removeReviews:)
    @NSManaged public func removeFromReviews(_ values: NSSet)

}
