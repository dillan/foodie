//
//  Formatters.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/9/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import Foundation

struct Formatter {
    
    /// A date formatter for presentation of full dates. For example, "Wednesday, June 10, 2020"
    static let fullDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }()
}
