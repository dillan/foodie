//
//  RestaurantViewController.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/7/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit
import CoreData

class RestaurantListViewController: UIViewController {
    
    @IBOutlet weak var sortSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var restaurants: [Restaurant] = []
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshData()
    }
    
    /// Refreshes the data presented in the UI with the users preferred sort order.
    private func refreshData() {
        let sortDescriptors: [NSSortDescriptor]
        
        let nameSort = NSSortDescriptor(key: "name",
                                        ascending: true,
                                        selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        
        switch sortSegmentedControl.selectedSegmentIndex {
        case 0:
            let reviewSort = NSSortDescriptor(key: "averageRating", ascending: false)
            sortDescriptors = [reviewSort, nameSort]
        case 1:
            sortDescriptors = [nameSort]
        case 2:
            let dateSort = NSSortDescriptor(key: "mostRecentReview", ascending: false)
            sortDescriptors = [dateSort, nameSort]
        default:
            sortDescriptors = [nameSort]
        }
        
        let fetchRequest: NSFetchRequest<Restaurant> = Restaurant.fetchRequest()
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            restaurants = try DataStore.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest) as [Restaurant]
        } catch {
            debugPrint("Error fetching restaurants: \(error)")
        }
        
        // Note: using reload data for expedience. Consider using NSFetchedResultsController for updates instead.
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Review" {
            if let indexPath = tableView.indexPathForSelectedRow,
                let restaurant = restaurantAtIndexPath(indexPath),
                let nextViewController = segue.destination as? ReviewListViewController {
                    nextViewController.restaurant = restaurant
            }
        }
    }
    
    @IBAction func sortSegmentedControlValueChanged(_ sender: Any) {
        refreshData()
    }
}

extension RestaurantListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let restaurant = restaurantAtIndexPath(indexPath) else { return nil }
        
        let deleteAction = UIContextualAction(style: .destructive, title: NSLocalizedString("Delete", comment: "Delete")) { [weak self] (_, _, completion) in
            if let context = restaurant.managedObjectContext {
                context.delete(restaurant)
                do {
                    try context.save()
                    self?.refreshData()
                    completion(true)
                } catch {
                    debugPrint("\(error)")
                    completion(false)
                }
            }
        }
        
        let editAction = UIContextualAction(style: .normal, title: NSLocalizedString("Edit", comment: "Edit")) { [weak self] (_, _, completion) in
            if let viewController = self?.storyboard?.instantiateViewController(identifier: "RestaurantViewController") as? RestaurantViewController {
                viewController.restaurant = restaurant
                self?.navigationController?.pushViewController(viewController, animated: true)
            }
            completion(true)
        }
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        
        return configuration
    }
}

extension RestaurantListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableViewCell.reuseIdentifier, for: indexPath) as? RestaurantTableViewCell else {
            fatalError("Dequeued cell not RestaurantTableViewCell")
        }
        
        configureCell(cell, forItemAt: indexPath)
        
        return cell
    }
    
    
    /// Applies model data to a cell for presentation in the UI.
    /// - Parameters:
    ///   - cell: A `RestaurantTableViewCell` to populate with data.
    ///   - indexPath: The `IndexPath` of the model data that will be used to populate the cell.
    private func configureCell(_ cell: RestaurantTableViewCell, forItemAt indexPath: IndexPath) {
        guard let restaurant = restaurantAtIndexPath(indexPath) else { return }
        
        cell.nameLabel.text = restaurant.name ?? "Unknown"
        cell.cuisineLabel.text = restaurant.cuisine?.name ?? "Unknown"
        cell.ratingLabel.text = String(format: "%.1f", restaurant.averageRating)
        cell.reviewCountLabel.text = "\(restaurant.reviews?.count ?? 0)"
    }
    
    /// Safely retrieves a 'Cuisine' object from a specified index path.
    /// - Parameter indexPath: The `IndexPath` where the object is expected.
    /// - Returns: If found, the `Cuisine` matching the specified index path, otherwise nil.
    func restaurantAtIndexPath(_ indexPath: IndexPath) -> Restaurant? {
        if restaurants.count > indexPath.row {
            let restaurant = restaurants[indexPath.row]
            return restaurant
        }
        
        return nil
    }
}
