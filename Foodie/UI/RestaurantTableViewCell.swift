//
//  RestaurantTableViewCell.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/7/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {
    static let reuseIdentifier = String(describing: RestaurantTableViewCell.self)

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cuisineLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var reviewCountHostingView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareForReuse()
        
        reviewCountHostingView.layer.cornerRadius = reviewCountHostingView.bounds.height / 2.0
        reviewCountHostingView.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = ""
        cuisineLabel.text = ""
        ratingLabel.text = "0.0"
        reviewCountLabel.text = "0"
    }
}
