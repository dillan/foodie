//
//  RestaurantViewController.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/7/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit
import CoreData

class RestaurantViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cuisinePicker: UIPickerView!
    
    private var cuisines: [Cuisine] = []
    
    var restaurant: Restaurant? {
        didSet {
            if isViewLoaded == true {
                updateUI()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let fetchRequest: NSFetchRequest<Cuisine> = Cuisine.fetchRequest()
        let sort = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            cuisines = try DataStore.sharedInstance.persistentContainer.viewContext.fetch(fetchRequest) as [Cuisine]
        } catch {
            debugPrint("Error fetching cuisines: \(error)")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        saveRestaurant()
    }
    
    /// Safely retrieves a 'Cuisine' object from a specified index path.
    /// - Parameter indexPath: The `IndexPath` where the object is expected.
    /// - Returns: If found, the `Cuisine` matching the specified index path, otherwise nil.
    private func cuisineAt(index: Int) -> Cuisine? {
        if index < cuisines.count {
            let cuisine = cuisines[index]
            return cuisine
        }
        
        return nil
    }
    
    /// Populates the user interface with current model data.
    private func updateUI() {
        nameTextField.text = restaurant?.name ?? ""
        
        if let cuisineIndex = cuisines.firstIndex(where: { $0.objectID == restaurant?.cuisine?.objectID }) {
            cuisinePicker.selectRow(cuisineIndex, inComponent: 0, animated: false)
        }
    }
    
    /// Saves the Restaurant model data to the managed object context.
    private func saveRestaurant() {
        guard let name = nameTextField.text, let cuisine = cuisineAt(index: cuisinePicker.selectedRow(inComponent: 0)) else { return }
        
        let context = DataStore.sharedInstance.persistentContainer.viewContext
        
        let aRestaurant = restaurant ?? Restaurant(entity: Restaurant.entity(), insertInto: context)
        if aRestaurant.id == nil {
            aRestaurant.id = UUID()
        }
        aRestaurant.name = name
        aRestaurant.cuisine = cuisine
        
        do {
            try context.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
}

extension RestaurantViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cuisines.count
    }
}

extension RestaurantViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let cuisine = cuisineAt(index: row) {
            return cuisine.name
        }
        return nil
    }
}
