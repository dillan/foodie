//
//  ReviewListViewController.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/7/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit

class ReviewListViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cuisineLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var restaurant: Restaurant? {
        didSet {
            refreshData()
        }
    }
    
    var reviews: [Review] = [] {
        didSet {
            if isViewLoaded == true {
                // Note: using reload data for expedience. Consider using NSFetchedResultsController for updates instead.
                tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshData()
        
        let reviewCount = restaurant?.reviews?.count ?? 0
        navigationItem.title = "Reviews (\(reviewCount))"
        
        nameLabel.text = restaurant?.name ?? "Unknown"
        cuisineLabel.text = restaurant?.cuisine?.name ?? "Unknown"
        ratingLabel.text = String(format: "%.1f\navg", restaurant?.averageRating ?? 0.0)
    }
    
    /// Refreshes the data presented in the UI descending date order.
    private func refreshData() {
        var updatedReviews = restaurant?.reviews?.allObjects as? [Review] ?? []
        updatedReviews = updatedReviews.sorted(by: {
            $0.date?.compare($1.date ?? Date.distantPast) == .orderedDescending
        })
        reviews = updatedReviews
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddReview",
            let nextViewController = segue.destination as? ReviewViewController {
            nextViewController.restaurant = restaurant
        }
    }
}

extension ReviewListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ReviewListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurant?.reviews?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.reuseIdentifier, for: indexPath) as? ReviewTableViewCell else {
            fatalError("Dequeued cell not ReviewTableViewCell")
        }
        
        configureCell(cell, forItemAt: indexPath)
        
        return cell
    }
    
    /// Applies model data to a cell for presentation in the UI.
    /// - Parameters:
    ///   - cell: A `ReviewTableViewCell` to populate with data.
    ///   - indexPath: The `IndexPath` of the model data that will be used to populate the cell.
    private func configureCell(_ cell: ReviewTableViewCell, forItemAt indexPath: IndexPath) {
        guard let review = reviewAtIndexPath(indexPath) else { return }
        
        if let date = review.date {
            cell.dateLabel.text = Formatter.fullDateFormatter.string(from: date)
        }
        cell.bodyLabel.text = review.notes ?? ""
        cell.ratingLabel.text = String(format: "%.1f", review.rating)
        
        cell.selectionStyle = .none
    }
    
    
    /// Safely retrieves a review from a specified index path.
    /// - Parameter indexPath: The `IndexPath` where the object is expected.
    /// - Returns: If found, the `Review` matching the specified index path, otherwise nil.
    func reviewAtIndexPath(_ indexPath: IndexPath) -> Review? {
        if reviews.count > indexPath.row {
            let review = reviews[indexPath.row]
            return review
        }
        
        return nil
    }
}
