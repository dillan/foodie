//
//  ReviewTableViewCell.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/9/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = String(describing: ReviewTableViewCell.self)
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        dateLabel.text = nil
        bodyLabel.text = nil
        ratingLabel.text = nil
    }
}
