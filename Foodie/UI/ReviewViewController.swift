//
//  ReviewViewController.swift
//  Foodie
//
//  Created by Dillan Laughlin on 6/9/20.
//  Copyright © 2020 Atigo Inc. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var restaurant: Restaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateRatingLabel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        saveReview()
    }
    
    /// Saves the review model data to the managed object context.
    private func saveReview() {
        guard let context = restaurant?.managedObjectContext else { return }
        
        let rating = ratingSlider.value
        let date = datePicker.date
        
        let review = Review(entity: Review.entity(), insertInto: context)
        review.rating = rating
        review.notes = reviewTextView.text
        review.date = date
        restaurant?.addReview(review)
        
        do {
            try context.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    /// Update the rating label with the current slider value.
    private func updateRatingLabel() {
        ratingLabel.text = String(format: "%.1f", ratingSlider.value)
    }
    
    @IBAction func ratingSliderValueChanged(_ sender: Any) {
        updateRatingLabel()
    }
}
