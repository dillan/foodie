# Foodie

Foodie is a sample project to illustrate competence in iOS development with the Swift programming language and key iOS frameworks.

## TODOs

There are a handful of tasks that remain to keep the time spent on the exercise close to the specified time box.

- Improve error handling.
- Add unit tests.
- Consider using NSFetchedResultsController to handle data updates to the UI.
